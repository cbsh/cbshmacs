;;; init.example.el --- The User's Init -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cameron@codecameron.dev>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: September 29, 2024
;; Modified: September 29, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  The User's Init file, this is the final step in loading CBSHMacs, this loads
;;  your private config, packages, etc.
;;
;;; Code:

;; this block is where the modules of CBSHMacs are configured, CBSHMacs needs to be restarted
;; for this to take effect!

;; This tells elpaca to load our configuration only after its completely loaded and
;; activated all the packages of CBSHMacs.
(add-hook 'elpaca-after-init-hook (lambda () (loadfile! "config.el")))

;; though our packages we set don't need to be told that, as we want these to be installed
;; when Elpaca is loading and activating our packages.
(loadfile! "packages.el")

(provide 'init)
;;; init.example.el ends here
