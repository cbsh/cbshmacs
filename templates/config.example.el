;;; config.example.el --- Example Configuration File -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;;; Commentary:
;;
;;  Example Configuration File for when CBSHMacs cannot find an existing config.
;;
;;; Code:
;;
;; If needed, you can setup identification with the following:
;; (setq user-full-name "Cameron Miller"
;;      user-mail-address "cabooshy@cabooshyy.xyz")


;; CBSHMacs has variables that store `font-spec's, which are used as follows:
;; (setq `cbshmacs-font'/`cbshmacs-mono-font'/`cbshmacs-variable-font' (font-spec :family "FONT" :size SIZE))
;; of course, the above won't work, its just an example. below are proper setq's you can use to change the font.
;;
;;(setq cbshmacs-font (font-spec :family "FONT" :size SIZE))
;;(setq cbshmacs-mono-font (font-spec :family "FONT" :size SIZE))
;;(setq cbshmacs-variable-font (font-spec :family "FONT" :size SIZE))

;; Load a theme:
(load-theme 'doom-outrun-electric t)

;; change how line numbers display
;; t -- line numbers are displayed
;; nil -- line numbers are not displayed
;; relative -- relative line numbers are displayed
(setq display-line-numbers-type 'relative)


;; if you use org, you can change the org directory here,
;; this needs to be set before org loads!
(setq org-directory "~/org/")

;;; config.example.el ends here
