;;; cbshmacs-lib.el--- The Common Lib for CBSHMacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 28, 2024
;; Modified: September 28, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;; cbshmacs-lib is the common library for CBSHMacs, it contains
;; all of the common functions that get used throughout CBSHMacs.
;;; Code:

;; Error Types
(define-error 'cbshmacs-err "An Unexpected CBSHMacs Error")
(define-error 'cbshmacs-font-err "Could not find specified font on your system" 'cbshmacs-err)
(define-error 'cbshmacs-core-err "CBSHMacs hasn't been initialised yet, have you ran Emacs yet?" 'cbshmacs-err)
(define-error 'cbshmacs-user-err "Error caused by user's config, or system." 'cbshmacs-err)
(define-error 'cbshmacs-module-err "Error in a CBSHMacs Module" 'cbshmacs-err)
(define-error 'cbshmacs-package-err "Error with packages" 'cbshmacs-err)


;;
;;; Logging

(defvar cbshmacs-inhibit-log (not (or noninteractive init-file-debug))
"If Non-nil, suppress `cbshmacs-log' output.")




(defmacro file! ()
  "Return the file of the file this macro was called."
  (or (bound-and-true-p byte-compile-current-file)
      load-file-name
      (buffer-file-name (buffer-base-buffer))
      (let ((file (car (last current-load-list))))
	(if (stringp file) file))
      (error "file!: Cannot deduce the current file path")))

(defmacro dir! ()
  "Return the directory of the file in which this macro was called."
  (or (let (file-name-handler-alist)
	(file-name-directory (macroexpand '(file!))))
      (error "dir!: Cannot deduce the current directory path")))

(defun cbshmacs-load (path &optional noerror)
"Load PATH and handle any errors that arise from it.

If NOERROR, do not throw an error if PATH does not exist."
(condition-case-unless-debug e
    (load path noerror 'nomessage)
(cbshmacs-err
 (signal (car e) (cdr e)))
(error
 (setq path (locate-file path load-path (get-load-suffixes)))
 (if (not (and path (featurep 'cbshmacs)))
     (signal (car e) (cdr e))
   (cl-loop for (err . dir)
	    in `((cbshmacs-core-err . ,cbshmacs-core-directory)
		 (cbshmacs-user-err . ,cbshmacs-user-directory)
		 (cbshmacs-module-err . ,cbshmacs-modules-directory))
	    if (file-in-directory-p path dir)
	    do (signal err (list (file-relative-name path (expand-file-name "../" dir))
				 e)))))))

(defmacro loadfile! (filename &optional path noerror)
 "Load a File relative to the current executing file.
FILENAME is either the file path or a form that should
evaluate to such a string at run time.  PATH is where
to look for the file (a string representing a directory path),
if omitted, the lookup is relative to:
- function `load-file-name'
- function `byte-compile-current-file',
- function `buffer-file-name'
\(in that order).

If NOERROR is non-nil, don't throw an error if the file does not exist."
`(cbshmacs-load
  (file-name-concat ,(or path `(dir!)) ,filename)
  ,noerror))



(defun cbshmacs-require (feature &optional filename noerror)
  "Like `require' but with extra error handling.
This is used for loading core modules before `cbshmacs-local-package'
is available."
  (let ((subfeature (if (symbolp filename) filename)))
    (or (featurep feature subfeature)
        (cbshmacs-load
         (if subfeature
             (file-name-concat cbshmacs-core-directory
                               (string-remove-prefix "cbshmacs-" (symbol-name feature))
                               (symbol-name filename))
           (symbol-name feature))
         noerror))))

(provide 'cbshmacs-lib)
;;; cbshmacs-lib.el ends here
