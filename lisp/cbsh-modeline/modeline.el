;;; modeline.el --- A Simple Modeline for CBSHMacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 19, 2024
;; Modified: September 19, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; cbsh-modeline is a simple Modeline for CBSHMacs, while i could just use the doom modeline, where's the fun in that?
;; where's the learning potential?
;;
;;; Code:

(defvar cbshmacs/mode-line-selected-window (frame-selected-window)
"Keep Track of the Selected Window, so we can render the Mode Line differently.")

(defun cbshmacs/mode-line-set-selected-window (&rest _args)
  (when (not (minibuffer-window-active-p (frame-selected-window)))
    (setq cbshmacs/mode-line-selected-window (frame-selected-window))
    (force-mode-line-update)))

(defun cbshmacs/mode-line-unset-selected-window ()
  (setq cbshmacs/mode-line-selected-window nil)
  (force-mode-line-update))

(add-hook 'window-configuration-change-hook #'cbshmacs/mode-line-set-selected-window)
(add-hook 'focus-in-hook #'cbshmacs/mode-line-set-selected-window)
(add-hook 'focus-out-hook #'cbshmacs/mode-line-unset-selected-window)

(advice-add 'handle-switch-frame :after #'cbshmacs/mode-line-set-selected-window)
(advice-add 'select-window :after #'cbshmacs/mode-line-set-selected-window)

(defun cbshmacs/mode-line-selected-window-active-p ()
  (eq cbshmacs/mode-line-selected-window (selected-window)))

(defvar cbshmacs/mode-line-evil-state-colours
  '((cbshmacs-mode-line-evil-normal "#0D1F73" "Evil normal state face.")
    (cbshmacs-mode-line-evil-insert "#7CEEF6" "Evil insert state face.")
    (cbshmacs-mode-line-evil-emacs "#C2124F" "Evil Emacs state face.")
    (cbshmacs-mode-line-evil-visual "#C9951B" "Evil visual state face.")))


(defface cbshmacs-default-face '((t (:foreground "white")))
  "Default Face for the Mode Line.")
(defface cbshmacs/line-modified-face '((t (:foreground "orange")))
"Custom Face for modified Buffers.")

(defvar cbshmacs/evil-state-faces
  '((normal . cbshmacs-mode-line-evil-normal)
    (visual . cbshmacs-mode-line-evil-visual)
    (insert . cbshmacs-mode-line-evil-insert)
    (emacs . cbshmacs-mode-line-evil-emacs)))

(defun cbshmacs/evil-state-face ()
  (if-let ((face (and
		  (bound-and-true-p evil-local-mode)
		  (assq evil-state
			(if (cbshmacs/mode-line-selected-window-active-p)
			    cbshmacs/evil-state-faces)))))
      (cdr face)
    'cbshmacs-default-face))

(setq mode-line-format nil)
(kill-local-variable 'mode-line-format)

(setq-default mode-line-format
	      (list
	       '(:eval (propertize (if (eq 'emacs evil-state) )
				   'face (cbshmacs/evil-state-face)))
	      " "
	      '(:eval (list
		       (propertize " %b" 'face 'font-lock-string-face)
		       (when (buffer-modified-p)
			 (propertize
			  " [+]" 'face 'cbshmacs/line-modified-face))
		       (when buffer-read-only
			 (propertize
			  " [RO]"
			  'face (if (cbshmacs/mode-line-selected-window-active-p)
				    'cbshmacs/line-read-only-face
				  'cbshmacs/line-read-only-face-inactive)))
		       " "))
	      
	      '(:eval (quote ("%m " (vc-mode vc-mode))))))


(provide 'cbsh-modeline)
;;; modeline.el ends here
