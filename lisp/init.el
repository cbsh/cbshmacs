;;; init.el -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;
;; :core is a normal module, so this is where it's init lives.
;;; Code:
(require 'packages (concat user-emacs-directory "modules/packages.el"))
(require 'cbshmacs-keybinds(concat user-emacs-directory "lisp/cbshmacs-keybinds.el"))

(unless (boundp 'safe-local-variable-directories)
  (defvar safe-local-variable-directories ())
  (define-advice hack-local-variables-filter (:around (fn variables dir-name) respect)
    (let ((enable-local-variables
           (if (delq nil (mapcar (lambda (dir)
                                   (and dir-name dir
                                        (file-equal-p dir dir-name)))
                                 safe-local-variable-directories))
               :all
             enable-local-variables)))
      (funcall fn variables dir-name))))

(add-to-list 'safe-local-variable-directories user-emacs-directory)
(add-to-list 'safe-local-variable-directories cbshmacs-user-directory)

(add-to-list 'auto-mode-alist '("/\\.cbshmacs\\(?:project\\|module\\)?\\'" . lisp-data-mode))
(add-to-list 'auto-mode-alist '("/\\.cbshmacsrc\\'" . emacs-lisp-mode))

(require 'user-init (concat cbshmacs-user-directory "init.el"))
(provide 'init)
;;; init.el ends here
