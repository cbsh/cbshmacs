;;; lisp/lib/config.el ---  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;;###autoload
(defun cbshmacs/set-font-faces ()
"Define the Font Faces to be used by Emacs frames."
(message "Setting Faces!")
;; These use the `cbshmacs-font', `cbshmacs-mono-font', and `cbshmacs-variable-font' vars which
;; contain a font spec.
(set-face-attribute 'default nil :font cbshmacs-font)
(set-face-attribute 'fixed-pitch nil :font cbshmacs-mono-font)
(set-face-attribute 'variable-pitch nil :font cbshmacs-variable-font))



;;;###autoload
(defun cbshmacs/reload ()
"Reloads your Configuration."
(interactive)

;; If the user has a literate config in $CBSHMACSDIR, tangle it here.
(if (file-readable-p (concat cbshmacs-user-directory "config.org"))
    (message "Tangling Config.")
    (org-babel-tangle-file (concat cbshmacs-user-directory "config.org")))

(message "Reloading Init.")
(load-file (concat user-emacs-directory "init.el"))

(message "Reloading Config.")
(load-file (concat cbshmacs-user-directory "config.el"))

(message "Setting Faces")
(cbshmacs/set-font-faces)

(message "Config Reloaded!"))

(provide 'config)
