;;; cbshmacs-modules.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 29, 2024
;; Modified: September 29, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;;
;;; Variables

(defvar cbshmacs-modules (make-hash-table :test 'equal)
"A Hash table of enabled modules. set by `cbshmacs-init-modules'.")


(defvar cbshmacs-module-load-path
  (list (file-name-concat cbshmacs-user-directory "modules")
	(file-name-concat user-emacs-directory "modules"))
"A List of paths where CBSHMacs will search for modules.

Order determines priority (highest to lowest).

Each entry is a string; an absolute path to the root directory of a module tree.
In Other Words, each module is a two-level nested structure, where the module's
group and name was deduced from the first and second level of directories.

For Example: if $CBSHMACSDIR/modules/ is an entry, a $CBSHMACSDIR/modules/lang/python/
directory represents a ':lang python' module.")


;;
;;; Module File Variables

(defvar cbshmacs-module-init-file "init.el"
"The filename for module early initialisation files.")

(defvar cbshmacs-module-config-file "config.el"
"The filename for module configuration files.")

(defvar cbshmacs-module-packages-file "packages.el"
"The filename for the package configuration file.")



;;
;;; Modules API
(defun cbshmacs-modulep (category module &optional flag)
"Return t if CATEGORY MODULE is enabled (ie.  present in `cbshmacs-modules')."
(declare (pure t) (side-effect-free t))
(when-let (plist (gethash (cons category module) cbshmacs-modules))
  (or (null flag)
      (and (memq flag (plist-get plist :flags))
	   t))))

(defun cbshmacs-moduledepth (category module &optional initdepth?)
"Return the depth of CATEGORY MODULE.

If INITDEPTH? is non-nil, use the CAR if a module was given two depths."
(if-let (depth (cbshmacs-getmodule category module :depth))
    (or (if initdepth?
	    (car-safe depth)
	  (cdr-safe depth))
	depth)
  0))

(defun cbshmacs-getmodule (category module &optional property)
"Return the plist for CATEGORY MODULE.  Gets PROPERTY, specifically, if set."
(declare (pure t) (side-effect-free t))
(when-let (plist (gethash (cons category module) cbshmacs-modules))
  (if property
      (plist-get plist property)
    plist)))


(defun cbshmacs-setmodule (category module &rest plist)
"Enables a Module by adding it to `cbshmacs-modules'.

CATEGORY is a keyword, MODULE is a symbol, PLIST is a plist
that accepts the following properties:

  :path STRING
  Path to the module directory.
  :depth INT|(INITDEPTH . CONFIGDEPTH)
  Determines the module load order.
  :flags (SYMBOL...)
  a list of activated Flags for this module.

If PLIST consists of a single nil, then the module is purged from memory instead."
(if (car plist)
    (let* ((depth (ensure-list (or (plist-get plist :depth) 0)))
	   (idepth (or (cdr depth) (car depth)))
	   (cdepth (car depth))
	   (idx (hash-table-count cbshmacs-modules)))
      (put category module
	   (vector idx idepth cdepth
		   category module
		   (plist-get plist :flags)))
      (puthash (cons category module) plist cbshmacs-modules))
  (remhash (cons category module) cbshmacs-modules)
(cl-remf (symbol-plist category) module)))





;;
;;; Module Configuration

(put :if     'lisp-indent-function 2)
(put :when   'lisp-indent-function 'defun)
(put :unless 'lisp-indent-function 'defun)

(defmacro cbshmacs! (&rest modules)
"Controls what modules are loaded."
`(when noninteractive
   (cbshmacs-module-mplist-map
    (lambda (category module &rest plist)
      (let ((path (cbshmacs-module-locate-path category module)))
	(unless path
	  (print (warn "Failed to locate a '%s %s' module") category module))
	(apply #'cbshmacs-setmodule category module
	       :path path
	       plist)))
    ,@(if (keywordp (car modules))
	  (list (list 'quote modules))
	modules))
   cbshmacs-modules))

;;
;;; Defaults

;; Register the :core and :user virtual modules, these represent the core of
;; CBSHMacs, and the user's config; which are always enabled.
(cbshmacs-setmodule :core nil :path cbshmacs-core-directory :depth -110)
(cbshmacs-setmodule :user nil :path cbshmacs-user-directory :depth '(-105 . 105))



(provide 'cbshmacs-modules)
;;; cbshmacs-modules.el ends here
