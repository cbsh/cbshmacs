;;; cbshmacs.el --- The Heart of CBSHMacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 28, 2024
;; Modified: September 28, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  The Heart of CBSHMacs, this is where the magic happens.
;;  this file is responsible for setting up everything related to CBSHMacs, bootstrapping Emacs, and
;;  loading CBSHMacs.
;;
;;; Code:

(require 'cbshmacs-lib (concat user-emacs-directory "lisp/cbshmacs-lib.el"))
(require 'cbshmacs-packages(concat user-emacs-directory "lisp/cbshmacs-packages.el"))
(require 'config(concat user-emacs-directory "lisp/lib/config.el"))


;;
;;; Core Globals:

(defgroup cbshmacs nil
"An Emacs Configuration for CBSH."
:tag "CBSHMacs"
:link '(url-link "https://gitlab.com/cbsh/cbshmacs")
:group 'emacs)


(defconst cbshmacs-version "1.0.0-a"
  "Current Version of CBSHMacs.")

(defvar cbshmacs-theme-directory (concat (getenv "HOME") "/.config/cbshmacs/themes/")
"The path to the CBSHMacs themes directory.  Must end in a slash.")

(defvar cbshmacs-modules-directory (concat user-emacs-directory "modules/")
"The path to the CBSHMacs modules directory.  Must end in a slash.")

(defconst cbshmacs-core-directory (dir!)
"The path to CBSHMacs' core files.  Must end in a slash.")

(defvar cbshmacs-template-config (concat user-emacs-directory "templates/config.example.el"))
(defvar cbshmacs-template-init (concat user-emacs-directory "templates/init.example.el"))
(defvar cbshmacs-template-packages (concat user-emacs-directory "templates/packages.example.el"))

(defvar cbshmacs-user-directory
  (expand-file-name
   (if-let (cbshmacsdir (getenv-internal "CBSHMACSDIR"))
       (file-name-as-directory cbshmacsdir)
     (or (let ((xdgdir
		(file-name-concat
		 (or (getenv-internal "XDG_CONFIG_HOME")
		     "~/.config")
		 "cbshmacs/")))
	   (if (file-directory-p xdgdir) xdgdir))
	 "~/.cbshmacs.d/")))
"Where your private config is placed.

Defaults to ~/.config/cbshmacs, ~/.cbshmacs.d, or the value set in the
CBSHMACSDIR envvar; whichever is found first.  Must end in a slash.")

(defvar cbshmacs-user-config (concat cbshmacs-user-directory "config.el")
"The path to the user's private config.")

(setq user-init-file (concat cbshmacs-user-directory "init.el"))

;; Scaffold user config if it does not exist.
(defun cbshmacs/generate-config ()
"Generate the default CBSHMacs Config, if it does not exist."
(if (not (file-exists-p cbshmacs-user-config))
    (progn
      (make-directory cbshmacs-user-directory)
      (copy-file cbshmacs-template-config cbshmacs-user-config)
      (copy-file cbshmacs-template-packages (concat cbshmacs-user-directory "packages.el"))
      (copy-file cbshmacs-template-init (concat cbshmacs-user-directory "init.el")))))

(add-to-list 'load-path cbshmacs-user-directory)
(add-to-list 'load-path cbshmacs-modules-directory)
(add-to-list 'custom-theme-load-path cbshmacs-theme-directory)



;;
;;; Custom Hooks

(defcustom cbshmacs-before-init-hook ()
  "A Hook that runs before $CBSHMACSDIR/init.el is loaded.

  This is triggered right before the user config is loaded in the
  context of `early-init.el'.  This is the final opportunity to
  apply configurations before the session becomes complicated with
  the user config, packages, etc."
  :group 'cbshmacs
  :type 'hook)

(defcustom cbshmacs-after-init-hook ()
  "A Hook that runs after:
- CBSHMacs' core is loaded
- CBSHMacs' modules are loaded
- user's config is loaded.

  This triggers at the latest point in the eager startup process."
  :group 'cbshmacs
  :type 'hook)


;;
;;; Last Minute init

(when (daemonp)
  (message "Starting CBSHMacs in Daemon Mode..."))


;;
;;; hand off to init to finish intitialisation process

(require 'init (concat cbshmacs-core-directory "init.el"))

(provide 'cbshmacs)
;;; cbshmacs.el ends here
