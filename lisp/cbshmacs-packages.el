;;; cbshmacs-packages.el --- Package Management in CBSHMacs. -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 28, 2024
;; Modified: September 28, 2024
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Package Management in CBSHMacs.
;;
;;; Code:
(defmacro cbshmacs-local-package! (PACKAGE &rest LOCATION)
"A Macro around `use-package' to load local files.
Accepts:
  `PACKAGE' as the package to load.
  (optional)`LOADPATH' for the `use-package' load path as a string.

This is used internally to load the modules and libraries, use `cbshmacs-elpkg!'
to install packages."
`(use-package ,PACKAGE
   :load-path ,LOCATION))

(defmacro cbshmacs-elpkg! (name &rest plist)
"Wraps around Elpaca to queue packages to be installed.
you can also pass in Elpaca's optional keywords to specify a
partial or full recipe.

Accepts:
  `NAME' -- The package to be installed
  (optional)`PLIST' -- Elpaca Keywords to specify a recipe.

Expands to:
  (elpaca PACKAGE) -- if only specifying the package name
  (elpaca (PACKAGE ARGS)) -- if specifying a partial or
  full recipe."
(if plist
     `(elpaca (,name ,plist)))
(if (not plist)
         `(elpaca ,name)))




(provide 'cbshmacs-packages)
;;; cbshmacs-packages.el ends here
