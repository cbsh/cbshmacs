;;; cbshmacs-keybinds.el --- The Default Keybinds -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cameron@codecameron.dev>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: September 29, 2024
;; Modified: September 29, 2024
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;  The Default Keybinds, as well as some setup to wrap around General to make
;;  it easier for the user to setup their own keybinds, as General, while very
;;  powerful, is a bit of a handful to setup if you don't know how to.
;;
;;; Code:



(provide 'cbshmacs-keybinds)
;;; cbshmacs-keybinds.el ends here
