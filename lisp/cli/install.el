;;; lisp/cli/install.el --- Package Management -*- lexical-binding: t; -*-
;;; Commentary:
;;  Package Management
;;; Code:

(load! "packages")

;;
;;; Commands

(defcli ((install i))
         ((aot? ("--aot") "Enable ahead-of-time native-comp (if available)")
          &flags
          (config? ("--config" :yes) "Create `$CBSHMACSDIR' or dummy files therein?")
          (install? ("--install" :yes) "Auto-install packages?")
          &context context)
         "Installs and sets up CBSHMacs for first use.")

(provide 'cbshmacs-cli-install)
;;; lisp/cli/install.el ends here
