;;; early-init.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cameron@codecameron.dev>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: September 17, 2024
;; Modified: September 17, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "27.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;; This is the first part of CBSHMacs that's run, so we use this to setup and tell things to not run before Emacs loads init.el, where it would be too late to do so.
;;
;;
;;; Code:
;;;

(setq user-emacs-directory (concat (getenv "HOME") "/.config/cbsh-macs/"))

;; Disable `package-quickstart' and `package-enable-at-startup' so that package.el is not present at all, allowing Elpaca to do all of the package management
(setq package-quickstart nil)
(setq package-enable-at-startup nil)


;; Defer native-comp so that it only happens as emacs loads elisp files, instead of all at once
;; Comment this out if you're not using a native-comp branch.
;;(setq comp-deferred-compilation t)

;; Defer GC further back in the startup process, so that it doesn't run while the config is loading.

;; Goodbye Startup Message
(setq inhibit-startup-message t)

;; Set Frame title to Reflect this config being used
(setq frame-title-format
      (concat "%b - CBSHMacs @ " (system-name)))

(scroll-bar-mode -1)                      ; Disable Scrollbar
(tool-bar-mode -1)                        ; Disable Toolbar
(tooltip-mode -1)                         ; Disable Tooltips
(set-fringe-mode 10)                      ; Add some Breathing room
(menu-bar-mode -1)                        ; Disable menu bar
(delete-selection-mode 1)                 ; Selecting text and then typing will delete the text.
(electric-indent-mode -1)                 ; electric's indenting is annoying anyway.
(electric-pair-mode 1)                    ; though electric's pair mode rules.
(setq org-edit-src-content-indentation 0) ; src blocks now start with an auto-indent of 0 instead of 2.
(setq use-file-dialog nil)                ; no file dialog pls, we have dired.
(setq use-dialog-box nil)                 ; dialog boxes are overrated.
(setq pop-up-windows nil)                 ; popup windows begone!

;; Add Visible Bell
(setq visible-bell t)

;; the following will make it so < does not autopair via electric when `electric-pair-mode' is on.
;; otherwise you need to delete the > to do `org-tempo' shorthands like <s TAB.
(add-hook 'org-mode-hook (lambda ()
			   (setq-local electric-pair-inhibit-predicate
				       `(lambda (c)
					  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))


(add-to-list 'load-path (expand-file-name (concat user-emacs-directory "lisp")))
(add-to-list 'load-path (expand-file-name (concat user-emacs-directory "lisp/lib")))

;;; early-init.el ends here
