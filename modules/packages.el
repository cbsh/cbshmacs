;;; packages.el --- module packages -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
;;;
(require 'cbshmacs-keys (concat cbshmacs-modules-directory "keybinds/cbshmacs-keys.el"))
(require 'cbshmacs-ui (concat cbshmacs-modules-directory "ui/cbshmacs-ui.el"))
(require 'cbshmacs-themes (concat cbshmacs-modules-directory "ui/cbshmacs-themes.el"))
(require 'cbshmacs-dev (concat cbshmacs-modules-directory "dev/cbshmacs-dev.el"))

(provide 'packages)
;;; packages.el ends here.
