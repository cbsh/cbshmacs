;;; ui.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cameron@codecameron.dev>
;; Maintainer: Cameron Miller <cameron@codecameron.dev>
;; Created: September 17, 2024
;; Modified: September 17, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
;;;
(defcustom cbshmacs-font nil
  "The Default font to use.
Must be a `font-spec', a font object, XFT font string, or an XLFD string
Examples:
(setq cbshmacs-font (font-spec :family \"Fira Code Nerd Font\" :size 12")

(defcustom cbshmacs-mono-font nil
  "The Default monospace font to use.
Must be a `font-spec', a font object, XFT font string, or an XLFD string
Examples:
(setq cbshmacs-mono-font (font-spec :family \"Fira Code Nerd Font\" :size 12")

(defcustom cbshmacs-variable-font nil
  "The Default variable pitch font to use.
Must be a `font-spec', a font object, XFT font string, or an XLFD string
Examples:
(setq cbshmacs-font (font-spec :family \"Fira Code Nerd Font\" :size 12")

(setq cbshmacs-font (font-spec :family "Fira Code Nerd Font" :size 16)
      cbshmacs-mono-font (font-spec :family "SauceCodePro Nerd Font" :size 14)
      cbshmacs-variable-font (font-spec :family "Fira Code Nerd Font" :size 16))

(set-face-attribute 'default nil :font cbshmacs-font)
(set-face-attribute 'fixed-pitch nil :font cbshmacs-mono-font)
(set-face-attribute 'variable-pitch nil :font cbshmacs-variable-font)

(use-package org-superstar
  :ensure t
  :hook (org-mode . org-superstar-mode))

(use-package solaire-mode
  :ensure t)

;; Rainbow Delimiters
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :ensure t)
(define-globalized-minor-mode global-rainbow-mode rainbow-mode
  (lambda ()
    (when (not (memq major-mode
		     (list 'org-agenda-mode)))
      (rainbow-mode 1))))
(add-hook 'elpaca-after-init-hook (lambda () (global-rainbow-mode 1)))

(use-package swiper
  :ensure t) ;; Swiper for better fuzzy search in buffers.
(use-package ivy
  :ensure t
  :diminish
  :bind (:map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("C-l" . ivy-alt-done)
	 ("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("C-k" . ivy-previous-line)
	 ("C-l" . ivy-done)
	 ("C-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("C-k" . ivy-previous-line)
	 ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-posframe
  :ensure t
  :after ivy)
(setq ivy-posframe-display-functions-alist
      '((swiper                     . ivy-posframe-display-at-frame-top-center)
	(complete-symbol            . ivy-posframe-display-at-frame-top-center)
	(counsel-M-x                . ivy-posframe-display-at-frame-top-center)
	(counsel-esh-history        . ivy-posframe-display-at-frame-top-center)
	(counsel-describe-function  . ivy-posframe-display-at-frame-top-center)
	(counsel-describe-variable  . ivy-posframe-display-at-frame-top-center)
	(counsel-find-file          . ivy-posframe-display-at-frame-top-center)
	(counsel-recentf            . ivy-posframe-display-at-frame-top-center)
	(counsel-register           . ivy-posframe-display-at-frame-top-center)
	(dmenu                      . ivy-posframe-display-at-frame-top-center)
	(t                        . ivy-posframe-display))
      ivy-posframe-height-alist
      '((swiper . 20)
	(t . 15)))
(add-hook 'elpaca-after-init-hook (lambda () (ivy-posframe-mode 1)))

(use-package persp-mode
  :ensure t
  :unless noninteractive
  :commands persp-switch-to-buffer
  :config
  (persp-mode 1))

(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
	 :map minibuffer-local-map
   	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil))

(use-package ivy-rich
  :ensure t
  :after ivy
  :init
  (ivy-rich-mode 1))

(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package which-key
  :ensure t
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.3))

(use-package doom-modeline
  :ensure t
  :hook (emacs-startup . doom-modeline-mode))
(use-package all-the-icons
  :ensure t)
(setq doom-modeline-height 30)

(use-package imenu-list
  :ensure t)
(setq imenu-list-focus-after-activation t)


;; Like Doom, CBSHMacs does not and will not support `customise'. Its a Clumsy interface
;; that likes to set variables at times where they can be overwritten, leading to them
;; being rendered null. Configure things via $CBSHMACSDIR instead.
(dolist (sym '(customize-option customize-browse customize-group customize-face
               customize-rogue customize-saved customize-apropos
               customize-changed customize-unsaved customize-variable
               customize-set-value customize-customized customize-set-variable
               customize-apropos-faces customize-save-variable
               customize-apropos-groups customize-apropos-options
               customize-changed-options customize-save-customized))
  (put sym 'disabled "CBSHMacs does not support `customise', configure Emacs from $CBSHMACSDIR/config.el instead"))
(put 'customize-themes 'disabled "Set themes through `load-theme' in $CBSHMACSDIR/config.el instead")

(use-package dashboard
  :ensure t
  :preface
    (defun cbshmacs/scratch-buffer ()
    "Create a *scratch* Buffer"
    (interactive)
    (switch-to-buffer (get-buffer-create "*scratch*"))
    (lisp-interaction-mode))
  :init
  (dashboard-setup-startup-hook)
  (setq dashboard-startupify-list '(dashboard-insert-banner
                                    dashboard-insert-newline
                                    dashboard-insert-banner-title
                                    dashboard-insert-newline
                                    dashboard-insert-navigator
                                    dashboard-insert-newline
                                    dashboard-insert-init-info
                                    dashboard-insert-items
                                    dashboard-insert-newline
                                    dashboard-insert-footer))
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "\nKEYBINDINGS:\
\nFind File               (SPC .)     \
Open Buffer List    (SPC b i)\
\nFind Recent Files       (SPC f r)   \
Open the EShell     (SPC e s)\
\nOpen Dired file manager (SPC d d)   \
List of keybindings (SPC h b b)
\nOpen Config             (SPC f c)   \
Open VTerm          (SPC t v)\
\nReload Config           (SPC h r r) \
Fuzzy Search        (SPC /)\
\nFunction Help           (SPC h f)   \
Variable Help       (SPC h v)")
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content nil)
  (setq dashboard-item-names '(("Recent Files:" . "Recently opened files:")
                               ("Agenda for today:" . "Today's Agenda:")
                               ("Agenda for the coming week:" . "Agenda:")))
  (setq dashboard-items '((recents . 10)
                          (bookmarks . 5)
                          (projects . 5)))
  (add-to-list 'dashboard-items '(agenda) t)
  (setq dashboard-week-agenda t)
  (setq dashboard-navigator-buttons
	`(;;line1
	  ((,nil
	    "CBSHMacs on Gitlab"
	    "Open the CBSHMacs Repo in your browser"
	    (lambda (&rest _) (browse-url "https://gitlab.com/cbsh/cbshmacs"))
	    'default)
	   (,nil
	    "Open Scratch Buffer"
	    "Switch to a Scratch Buffer"
	    (lambda (&rest _) (cbshmacs/scratch-buffer))
	    'default)))))

(use-package org-indent
  :ensure nil)

(defun cbshmacs/daemon-fonts ()
  "Set the Font Faces for the Daemon, to be applied to client frames."
  (message "Setting Daemon Faces.")
  (set-face-attribute 'default nil :font cbshmacs-font)
  (set-face-attribute 'fixed-pitch nil :font cbshmacs-mono-font)
  (set-face-attribute 'variable-pitch nil :font cbshmacs-variable-font))

(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (lambda (frame)
		(with-selected-frame frame
		  (cbshmacs/daemon-fonts))))
  (cbshmacs/daemon-fonts))



(provide 'cbshmacs-ui)
;;; ui.el ends here
