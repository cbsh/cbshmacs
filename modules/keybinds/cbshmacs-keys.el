;;; keys.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Cameron Miller
;;
;; Author: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Maintainer: Cameron Miller <cabooshy@cabooshyy.xyz>
;; Created: September 17, 2024
;; Modified: September 17, 2024
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Setup Keybinds here, will also have a way to map keys later on.
;;
;;; Code:
(use-package general
  :ensure t
  :config
  (general-evil-setup t)
  (general-create-definer cbsh/leader-keys
    :keymaps '(normal visual emacs)
    :prefix "SPC"
    :non-normal-prefix "M-SPC")

  (cbsh/leader-keys
   ;; File Navigation
   "."   '(counsel-find-file :which-key "Find a file on the system")
   "/"   '(swiper :which-key "Launch Swiper (SWIPER NO SWIPING)")
   "RET" '(counsel-bookmark :which-key "Jump to Bookmark")
   "fc"      '((lambda () (interactive) (find-file "~/.config/cbshmacs/config.org")):wk "Open Config File")
   "fr"      '(counsel-recentf :wk "Find recently opened files" )
   "TAB TAB" '(comment-line :wk "Comment Lines"))

  (cbsh/leader-keys
   ;; Frame Bindings
   "q"  '(:ignore q :which-key "Frame Control")
   "qf" '(delete-frame :which-key "Delete (close) the current Frame")
   "qF" '())

  (cbsh/leader-keys
   ;; Buffer Bindings
   "b"   '(:ignore b :which-key "Buffer Control")
   "bb"  '(switch-to-buffer :which-key "Switch Buffer")
   "bm"  '(bookmark-set :which-key "Bookmark Location")
   "bM"  '(bookmark-delete :which-key "Delete Bookmark")
   "bi"  '(ibuffer :which-key "Launch iBuffer")
   "bn"  '(evil-next-buffer :which-key "Go to Next Buffer")
   "bp"  '(evil-prev-buffer :which-key "Go to Previous Buffer")
   "bk"  '(evil-delete-buffer :which-key "Delete Buffer")
   "bc"  '(evil-buffer-new :which-key "Create new empty Buffer"))

  (cbsh/leader-keys
   ;; Window Controls
   "w"  '(:ignore w :which-key "Window Controls")
   "ws" '(evil-window-split :which-key "Horizontal Split")
   "wv" '(evil-window-vsplit :which-key "Vertical Split")
   "wc" '(evil-window-delete :which-key "Delete Window")
   "ww" '(evil-window-next :which-key "Next Window")
   "wh" '(evil-window-left :which-key "Move Cursor to Left Window")
   "wj" '(evil-window-down :which-key "Move Cursor to Lower Window")
   "wk" '(evil-window-up :which-key "Move Cursor to Upper Window")
   "wl" '(evil-window-right :which-key "Move Cursor to Right Window")
   "wH" '(evil-window-move-far-left :which-key "Move Window Position Left")
   "wJ" '(evil-window-move-very-bottom :which-key "Move Window Position Down")
   "wK" '(evil-window-move-very-top :which-key "Move Window Position Up")
   "wL" '(evil-window-move-far-right :which-key "Move Window Position Right"))

  (cbsh/leader-keys
   ;; Help Bindings
   "h"   '(:ignore h :which-key "Help")
   "hf"  '(describe-function :which-key "Help Describe a Function")
   "hv"  '(describe-variable :which-key "Help Describe a Variable")
   "hb"  '(describe-bindings :which-key "Help Describe Bindings")
   "hi"  '(info :which-key "General Information")
   "hrr" '(cbshmacs/reload :wk "Reload Emacs Config"))

  (cbsh/leader-keys
   ;; Org Binds
   "m"  '(:ignore m :which-key "Org Mode")
   "mB" '(org-babel-tangle :which-key "Export Source Blocks from org document to file")
   "M-i" '(completion-at-point :which-key "Do Completion at Point of cursor"))

  (cbsh/leader-keys
   ;; Org Roam
   "n"  '(:ignore n :which-key "Org Roam")
   "nl" '(org-roam-buffer-toggle :which-key "Toggle Org Roam Buffer")
   "nf" '(org-roam-node-find :which-key "Find a Node")
   "ni" '(org-roam-node-insert :which-key "Insert a Node"))

  (cbsh/leader-keys
   ;; Toggles
   "t"  '(:ignore t :which-key "Toggles")
   "tr" '(read-only-mode :which-key "Toggle Read Only Mode")
   "tt" '(counsel-load-theme :which-key "Choose Theme")
   "tv" '(vterm :which-key "Open Vterm")
   "ti" '(imenu-list-smart-toggle :which-key "Toggle Imenu shown in a sidebar")
   "tw" '(visual-line-mode :wk "Toggle Truncated Lines (Word Wrapping)")
   "tl" '(display-line-numbers-mode :wk "Toggle Line Numbers")
   "tp" '(ivy-posframe-mode :wk "Toggle Ivy Posframe Mode"))

  (cbsh/leader-keys
   ;; IMenu
   "s"  '(:ignore s :which-key "Search")
   "si" '(counsel-imenu :which-key "Menu to quickly jump to places in a buffer"))

  (cbsh/leader-keys
   ;; Evaluation
   "e"  '(:ignore e :wk "Evaluate")
   "eb" '(eval-buffer :wk "Evaluate elisp in Buffer")
   "ed" '(eval-defun :wk "Evaluate defun containing or after point")
   "ee" '(eval-expression :wk "Evaluate an elisp expression")
   "el" '(eval-last-sexp :wk "Evaluate elisp expression before point")
   "er" '(eval-region :wk "Evaluate elisp in region"))

  (cbsh/leader-keys
   ;; Projectile
   "p"  '(:ignore p :wk "Projectile Commands")
   "pf" '(projectile-find-file :wk "Find a file within a project")
   "pd" '(projectile-find-dir :wk "Find a Project Directory")
   "pD" '(projectile-dired :wk "use Dired with Projectile")
   "pe" '(projectile-recentf :wk "Find Recently opened files in Projects")
   "pb" '(projectile-switch-to-buffer :wk "switch project buffers")
   "pv" '(projectile-run-vterm :wk "Run Vterm for a Project")
   "pc" '(projectile-compile-project :wk "Compile a Project, if applicable")
   "pu" '(projectile-run-project :wk "Run a Project, if apllicable")))

 (use-package evil
    :ensure t
    :demand t
    :init
    (setq evil-want-integration t)
    (setq evil-want-keybinding nil)
    (setq evil-want-C-u-scroll t)
    (setq evil-want-C-i-jump nil)
    :config
    (evil-mode 1)
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

    (evil-global-set-key 'motion "j" 'evil-next-visual-line)
    (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

    (evil-set-initial-state 'messages-buffer-mode 'normal)
    (evil-set-initial-state 'dashboard-mode 'normal))

  (use-package evil-collection
    :ensure t
    :after evil
    :config
    (evil-collection-init))

(provide 'cbshmacs-keys)
;;; keys.el ends here
