* Getting Started
GNU Emacs, everybody's favourite Elisp interpreter that just so happens to also edit text. Learning GNU Emacs is a
big adventure, so this getting started guide is help you get set up, using, configuring, and most importantly,
troubleshooting, to help make start of your Emacs Journey as smooth as it possibly can be.

* Table of Contents :toc:
- [[#getting-started][Getting Started]]
- [[#install][Install]]
- [[#emacs-and-dependencies][Emacs and Dependencies]]
  - [[#linux][Linux]]
  - [[#cbshmacs][CBSHMacs]]
  - [[#externalsystem-dependencies][External/System Dependencies]]
- [[#updating-and-rollback][Updating and Rollback]]
  - [[#rollback][Rollback]]
  - [[#updowngrading-emacs][Up/Downgrading Emacs]]
- [[#migration][Migration]]
- [[#configuration][Configuration]]
- [[#troubleshooting][Troubleshooting]]

* Install
By the end of this section, you will have installed:
- Git 2.23+
- Emacs 27.1+
- GNU Find
- (Optional) fd 7.3.0+, which will improve performance for many of the file indexing commands.

These packages should be available through the package manager of your OS, e.g. Homebrew & MacPorts on macOS,
scoop & chocolatey on Windows, or pacman/aptitude/etc. on the many Linux Distributions out there.


* Emacs and Dependencies
** Linux
Below are Installation instructions for the more popular Linux Distributions, if, in the case Emacs 27.1 or newer is
unavailable on your system, you will need to [[https://www.gnu.org/software/emacs/manual/html_node/efaq/Installing-Emacs.html][Build Emacs From Source]] instead.
*** Ubuntu
Emacs 27.x is not available out of the box in Ubuntu's Package Manager, but is available through a PPA:
#+begin_src bash
apt-get-repository ppa:kelleyk/emacs
apt-get update
apt-get install emacs27
#+end_src

Or, you can install Emacs via Snap:
#+begin_src bash
snap install emacs --classic
#+end_src

Sometimes, due to apt being, well, apt, you may need to uninstall the older version of emacs and its dependencies first,
and then install emacs27
#+begin_src bash
sudo apt remove emacs
sudo apt autoremove
#+end_src
**** Other Dependencies
Thenm we can install the other Dependencies that CBSHMacs needs:
#+begin_src bash
apt-get install fd-find

# for 18.04 and older, fd-find will not be available in
# the official repos. You will need to install them another
# way, such as the below:
sudo dpkg -i fd_8.2.1.amd64.deb # make sure to adapt version number and architechture to your system and version downloaded
#+end_src

*** Fedora
#+begin_src bash
  # required dependencies
  dnf install emacs git
  # optional dependencies
  dnf install fd-find # This is 'fd' in Fedora <28
#+end_src

*** Arch Linux
At the time of writing, the below will install Emacs 29.1
#+begin_src bash
  # required dependencies
  pacman -S git emacs #if you want native compilation instead, swap 'emacs' for 'emacs-nativecomp'
  #optional dependencies
  pacman -S fd
#+end_src

** CBSHMacs

** External/System Dependencies

* Updating and Rollback

** Rollback

** Up/Downgrading Emacs

* Migration

* Configuration

* Troubleshooting
